#!/usr/bin/env python3

from gi.repository import Gtk
x = None
class HistoryView(Gtk.TreeView):
    def __init__(self):
        super().__init__()
        self.store = Gtk.TreeStore(str, str)
        self.set_model(self.store)
        uri_renderer = Gtk.CellRendererText()
        binddn_renderer = Gtk.CellRendererText()
        self.connect("button-press-event", self.on_button)

        column = Gtk.TreeViewColumn()
        column.pack_start(uri_renderer, True)
        column.pack_start(binddn_renderer, True)

        column.add_attribute(uri_renderer, "markup", 0)
        column.add_attribute(binddn_renderer, "markup", 1)

        self.append_column(column)

        for i in range(1,5):
            global x
            x = self.store.append(None, ["ldap://benocs.com/dc=benocs,dc=com", "cn=admin,dc=benocs,dc=com"])
            print(x)

    def on_button(self, widget, event):
        if (event.button == 1 or event.button == 2):
            return False
        path = widget.get_path_at_pos(event.x, event.y)
        menu = Gtk.Menu()
        menu.attach_to_widget(self, None)
        menu.add(Gtk.MenuItem(label="Test"))
        menu.show_all()
        menu.popup(None, None, None, None, event.button, event.time)
        
  
window = Gtk.Window()
window.set_size_request(300, 300)
window.connect("destroy", Gtk.main_quit)
history_view = HistoryView()
window.add(history_view)
window.show_all()

Gtk.main()
