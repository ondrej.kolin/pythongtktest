#!/usr/bin/env python3

from gi.repository import Gtk,GLib
import threading
import socket
from time import sleep

import socket, time
from threading import Thread

class UserSearcher(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        print("Thread: User searching is will start now!")
        self.__tcpListener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__tcpListener.connect(("example.com", 389))
        while True:
            try:
                self.__tcpListener.sendall(b'Potato\n')
            except:
                print("Searching for chat members failed! Is the system shutting down?")
                break
        print ("End of run")

    def stop(self):
        self.__tcpListener.shutdown(socket.SHUT_RDWR)
       
class Window(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_size_request(300, 300)
        self.connect("destroy", Gtk.main_quit)
        self.button = Gtk.Button("Stop")
        self.button.connect("clicked", self.startstop)
        self.add(self.button)
        self.t = UserSearcher()
        self.t.start()


    def startstop(self, widget):
        self.t.stop()
        print(self.t.is_alive())


window = Window()        
window.show_all()

Gtk.main()
