#!/usr/bin/env python3

from gi.repository import Gtk, GObject
schema = {"history": []}


class HistoryViewRow(Gtk.ListBoxRow):
    connection = None
    check_button = None

    def __init__(self, connection=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connection = connection
        self.connect("activate", self.on_activated)
        self._build_widget()

    def on_activated(self, active):
        print("From self", active)
        pass

    def _build_widget(self):
        self.check_button = Gtk.CheckButton()
        self.check_button.set_can_focus(False)
        self.check_button.connect("toggled", self.on_checkbox_toggled)
        hbox = Gtk.HBox()
        vbox = Gtk.VBox()
        hbox.pack_start(self.check_button, 5, 5, True)
        hbox.pack_start(vbox, 5, 5, True)
        label = Gtk.Label()
        label.set_text("Test")
        vbox.pack_start(label, 5, 5, False)
        label = Gtk.Label()
        label.set_text("Test")
        vbox.pack_start(label, 5, 5, False)
        self.add(hbox)
        self.show_all()
        self.set_selectable(False)

    def on_checkbox_toggled(self, checkbox):
        self.emit("activate")

    def set_selectable(self, selectable):
        self.check_button.set_visible(selectable)

    def set_selected(self, selected):
        print("selected")
        if self.get_selected() == selected:
            return
        self.check_button.set_active(selected)

    def get_selected(self):
        self.check_button.get_active()


class HistoryView(Gtk.ListBox):
    #    __gsignals__ = {
    #        'entry-selected': (GObject.SIGNAL_ACTION, None, (Connection, ))
    #    }
    select_all = False

    def __init__(self):
        super().__init__()
        self.connect("row-activated", self.on_row_activated)

    def clear(self):
        for row in self:
            print(row)
            # TODO CLEAR

    # Signals
    def on_row_activated(self, widget, row):
        print(self.get_selected_rows())
        pass
        # iter = self.store.get_iter(path)
        # self.emit("entry-selected", self.store.get_value(iter, 2))

    def selection_mode(self, select_all):
        self.select_all = select_all
        if select_all:
            self.set_selection_mode(Gtk.SelectionMode.MULTIPLE)
        else:
            self.set_selection_mode(Gtk.SelectionMode.SINGLE)
        for row in self:
            row.set_selectable(select_all)

    def _load_data(self):
        for i in range(0, 5):
            row = HistoryViewRow()
            self.add(row)


window = Gtk.Window()
window.set_size_request(300, 300)
window.connect("destroy", Gtk.main_quit)
history_view = HistoryView()
window.add(history_view)
window.show_all()
history_view._load_data()
history_view.selection_mode(True)
Gtk.main()
