#!/usr/bin/env python3

from gi.repository import Gtk

class HistoryView(Gtk.TreeView):
    def __init__(self):
        super().__init__()
        self.store = Gtk.TreeStore(str, str)
        self.set_model(self.store)
        uri_renderer = Gtk.CellRendererText()
        binddn_renderer = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn()
        column.pack_start(uri_renderer, True)
        column.pack_start(binddn_renderer, True)

        column.add_attribute(uri_renderer, "markup", 0)
        column.add_attribute(binddn_renderer, "markup", 1)

        column.get_area().set_orientation(Gtk.Orientation.VERTICAL)

        self.append_column(column)

        self.store.append(None, ["ldap://benocs.com/dc=benocs,dc=com", "cn=admin,dc=benocs,dc=com"])

  
window = Gtk.Window()
window.set_size_request(300, 300)
window.connect("destroy", Gtk.main_quit)
history_view = HistoryView()
window.add(history_view)
window.show_all()

Gtk.main()
