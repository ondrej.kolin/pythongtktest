#!/usr/bin/env python3
from gi.repository import Gio

'''
void main () {
    var host = "www.google.com";

    try {
        // Resolve hostname to IP address
        var resolver = Resolver.get_default ();
        var addresses = resolver.lookup_by_name (host, null);
        var address = addresses.nth_data (0);
        print (@"Resolved $host to $address\n");

        // Connect
        var client = new SocketClient ();
        var conn = client.connect (new InetSocketAddress (address, 80));
        print (@"Connected to $host\n");

        // Send HTTP GET request
        var message = @"GET / HTTP/1.1\r\nHost: $host\r\n\r\n";
        conn.output_stream.write (message.data);
        print ("Wrote request\n");

        // Receive response
        var response = new DataInputStream (conn.input_stream);
        var status_line = response.read_line (null).strip ();
        print ("Received status line: %s\n", status_line);

    } catch (Error e) {
        stderr.printf ("%s\n", e.message);
    }
}
'''
host = "127.0.0.1"
resolver = Gio.Resolver.get_default()
address = resolver.lookup_by_name(host, None)[0]
print(f"Resolved to {address.to_string()}")
client = Gio.SocketClient()
conn = client.connect(Gio.InetSocketAddress.new(address, 80));
message = f"GET / HTTP/1.1\r\nHost: {address.to_string()}\r\n\r\n"
conn.get_output_stream().write (message.encode())
print("Send out")
response = conn.get_input_stream();
